import pygame
import time
import os
import numpy as np
import random
import math

GAMEWIDTH = 640
GAMEHEIGHT = 480
GAMESIZE = (GAMEWIDTH, GAMEHEIGHT)
PLAYER_RADIUS = 4
PLAYER_COUNT = 3
SCALE_FACTOR = 1

DISC_GAMEWIDTH = int(GAMEWIDTH / SCALE_FACTOR)
DISC_GAMEHEIGHT = int(GAMEHEIGHT / SCALE_FACTOR)

WINDOW_SIZE = (GAMEWIDTH *2, GAMEHEIGHT * 2)

player_keys = [
  pygame.K_LEFT, pygame.K_RIGHT,
  pygame.K_LSHIFT, 167,
  pygame.K_COMMA, pygame.K_PERIOD
  # pygame.K_LEFTBRACKET, pygame.K_RIGHTBRACKET
]

class Player():
  velocity = 60.0
  position = [0.0, 0.0]
  angle = 0.0
  rotation_speed = 2
  desired_direction = 0
  alive = True

  def discrete_position(self):
    return [int(self.position[0] / SCALE_FACTOR), int(self.position[1] / SCALE_FACTOR)]

players = []

COLORS = [
  (255, 39, 0),
  (195, 195, 0),
  (255, 121, 0),
  (1, 203, 2),
  (223, 80, 182),
  (0, 162, 203)
]

ticker_index = 0

COLORMAP = {}
GREYSCALE_INCREMENT = int(255 / PLAYER_COUNT)

Z = np.zeros(1)
UPDATE_INDEX = np.zeros(1)

def reset_game():
  initialize_players()
  initialize_field()

def initialize_players():
  global players
  players = []

  for player_index in range(PLAYER_COUNT):
    new_player = Player()

    new_player.position = [0.0, 0.0]
    new_player.position[0] = GAMEWIDTH/4 + GAMEWIDTH/2 * random.random()
    new_player.position[1] = GAMEHEIGHT/4 + GAMEHEIGHT/2 * random.random()
    new_player.angle = math.pi * 2 * random.random()

    players.append(new_player)

    COLORMAP[(player_index + 1) * GREYSCALE_INCREMENT] = COLORS[player_index]

def initialize_field():
  global Z
  global UPDATE_INDEX
  Z = (np.zeros((int(GAMEWIDTH / SCALE_FACTOR), int(GAMEHEIGHT / SCALE_FACTOR), 1))).astype('uint8')
  # duplicate across axis to get RGB
  Z = np.repeat(Z, 3, -1)
  UPDATE_INDEX = (np.zeros((int(GAMEWIDTH / SCALE_FACTOR), int(GAMEHEIGHT / SCALE_FACTOR)))).astype('int64')

def update_players(dt):

  # if False:
  for idx, player in enumerate(players):

    if player.alive:
      player.position[0] += player.velocity * math.cos(player.angle) * dt
      player.position[1] += player.velocity * math.sin(player.angle) * dt
      player.angle += player.rotation_speed * dt * player.desired_direction

def draw_players():

  global ticker_index

  for idx, player in enumerate(players):

    player_position_int = player.discrete_position()

    if player.alive:
      # draw square at player position with PLAYER_RADIUS as width
      player_x_start = int(player_position_int[0] - PLAYER_RADIUS/2)
      player_x_end = int(player_position_int[0] + PLAYER_RADIUS/2)
      player_y_start = int(player_position_int[1] - PLAYER_RADIUS/2)
      player_y_end = int(player_position_int[1] + PLAYER_RADIUS/2)

      for x in range(player_x_start, player_x_end + 1):
        for y in range(player_y_start, player_y_end + 1):

          # wall checking
          if x < 0 or x > (DISC_GAMEWIDTH-1) or y < 0 or y > (DISC_GAMEHEIGHT-1):
            player.alive = False
            continue

          # draw snake
          OWN_COLOR = (idx + 1) * GREYSCALE_INCREMENT
          
          # (Z[x,y,0] == OWN_COLOR and (ticker_index - UPDATE_INDEX[x,y]) > 10)
          if (Z[x,y,0] != OWN_COLOR and Z[x,y,0] != 0) or (Z[x,y,0] == OWN_COLOR and (ticker_index - UPDATE_INDEX[x,y]) > 10):
            
            # DEBUG
            # print("DIED BECAUSE: " + str(ticker_index - UPDATE_INDEX[x,y]))
            # print("DIED BECAUSE: " + str(UPDATE_INDEX[x,y]))
            # print("DIED BECAUSE: " + str(Z[x,y,0]))
            # print("AT: ", x, y)
            player.alive = False

          if player.alive:
            UPDATE_INDEX[x, y] = ticker_index
            Z[x,y,0] = OWN_COLOR
        

    # Z[player_x_start:player_x_end,player_y_start:player_y_end, 0] = (idx + 1) * GREYSCALE_INCREMENT

def color_Z(Z):
  col1_Z = np.copy(Z[:,:,0])
  col2_Z = np.copy(Z[:,:,1])
  col3_Z = np.copy(Z[:,:,2])

  # for each channel
  for k, v in COLORMAP.items():
    col1_Z[col1_Z == k] = v[0]
    col2_Z[col2_Z == k] = v[1]
    col3_Z[col3_Z == k] = v[2]
  
  stacked = np.stack([col1_Z, col2_Z, col3_Z], -1)
  return stacked

def gameloop(dt, screen):

  global WINDOW_SIZE
  
  # pos = pygame.mouse.get_pos()
  update_players(dt)
  draw_players()

  # update 1st channel to 2nd, and 3rd
  assign_RGB(Z, Z[:,:,0])

  colored_Z = color_Z(Z)

  surf = pygame.surfarray.make_surface(colored_Z)
  surf_scaled = pygame.transform.scale(surf, (WINDOW_SIZE[0], WINDOW_SIZE[1]))

  screen.blit(surf_scaled, (0,0))
  pygame.display.update()

def assign_RGB(Z, data2d):
  Z[:,:,0] = data2d
  Z[:,:,1] = data2d
  Z[:,:,2] = data2d

def main():

  global ticker_index
  global WINDOW_SIZE

  os.environ['SDL_VIDEO_CENTERED'] = '1'
  pygame.init()

  pygame.display.set_caption("ACHTUNG!")
  screen = pygame.display.set_mode((WINDOW_SIZE[0], WINDOW_SIZE[1]), pygame.RESIZABLE)

  # print(pygame.display.get_driver())
  running = True
  lastFrameTime = time.time()
  cumulativeDt = 0
  screen.fill((255,255,255))

  test_keys = [pygame.K_a, pygame.K_s, pygame.K_d, pygame.K_f, pygame.K_g, pygame.K_h, pygame.K_h, pygame.K_j, pygame.K_k, pygame.K_l]

  custom_keys_pressed = {}

  for test_key in test_keys:
    custom_keys_pressed[test_key] = False

  currentTime = time.time()

  while running:

    # for event in pygame.event.get():
    for event in pygame.event.get():
      if event.type == pygame.KEYDOWN:

        if event.key in test_keys:
          custom_keys_pressed[event.key] = True

        if event.key == pygame.K_SPACE:

          # players_alive = False
          # for player in players:
          #   if player.alive:
          #     players_alive = True
          #     break

          # if not players_alive:
          reset_game()

      if event.type == pygame.KEYUP:
        if event.key in test_keys:
          custom_keys_pressed[event.key] = False

      if event.type == pygame.VIDEORESIZE:
        screen = pygame.display.set_mode(event.dict['size'], pygame.RESIZABLE)

        WINDOW_SIZE = event.dict['size']

      if event.type == pygame.QUIT:
        running = False
        
    

    # keys_pressed = pygame.key.get_pressed()

    # for player_index in range(PLAYER_COUNT):
      
    #   left_pressed = keys_pressed[player_keys[player_index*2]]
    #   right_pressed = keys_pressed[player_keys[player_index*2 + 1]]
    #   if left_pressed:
    #     players[player_index].desired_direction = -1
    #   if right_pressed:
    #     players[player_index].desired_direction = 1
    #   if (not right_pressed and not left_pressed) or (left_pressed and right_pressed):
    #     players[player_index].desired_direction = 0

    ## timing code
    currentTime = time.time()
    dt = currentTime - lastFrameTime
    lastFrameTime = currentTime
    cumulativeDt += dt

    if cumulativeDt > (1/10):
      ticker_index += 1
      gameloop(cumulativeDt, screen)
      cumulativeDt = 0

      for keycode, value in custom_keys_pressed.items():
        print(str(keycode) + ": ", int(value), "\t", end='')

      print('')

      # if event.type == pygame.KEYDOWN or event.type == pygame.KEYUP:
      #     if event.key in player_keys:
      #       key_index = player_keys.index(event.key)
      #       player_index = int(key_index / 2)
      #       direction_index = (key_index % 2) * 2 - 1

            # keys_pressed = pygame.key.get_pressed()

            # for player_index in range(PLAYER_COUNT):
            #   if event.type == pygame.KEYUP:
            #     if not pygame.key.get_pressed()[player_keys[other_key_index]]:
            #       players[player_index].desired_direction = 0
            #   else:
            #     players[player_index].desired_direction = direction_index

            # other_key_index = key_index + ((key_index % 2) * 2 - 1) * -1

            # guard for player count
            # if player_index < PLAYER_COUNT:
              
    

if __name__ == "__main__":
  
  reset_game()

  main()
