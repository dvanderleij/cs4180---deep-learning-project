from gym.envs.registration import register

register(
    id='miniachtung-v0',
    entry_point='gym_miniachtung.envs:MiniAchtungEnv',
)