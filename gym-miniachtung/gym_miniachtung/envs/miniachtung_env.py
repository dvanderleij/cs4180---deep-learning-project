import gym
from gym import error, spaces, utils
from gym.utils import seeding
from gym.envs.toy_text import discrete
import random
from enum import Enum
import numpy as np

class Turn(Enum):
    NO_TURN = 0
    LEFT = 1
    RIGHT = 2


class Direction(Enum):
    NORTH = 0
    EAST = 1
    SOUTH = 2
    WEST = 3


class MiniAchtungEnv(gym.Env):
    metadata = {'render.modes': ['human']}
    W, H = 10, 10
    NUMBER_OF_PLAYERS = 1
    DEATH_REWARD = -100
    SURVIVE_STEP_REWARD = 1
    ENEMY_DEATH_REWARD = 20
    STARTING_POS_1P = [5, 5]
    STARTING_POS_2P = [[2, 5], [7, 5]]

    def __init__(self):
        # self.numberOfPlayers = NUMBER_OF_PLAYERS TODO find a way to set this via constructor
        self.t = 0
        self.board = [[0 for x in range(self.W)] for y in range(self.H)]
        self.players = []
        usedCoords = []
        
        def generateCoords():
            return [random.randint(1, self.W-2), random.randint(1, self.H-2)]

        for i in range(self.NUMBER_OF_PLAYERS):
        #     randomCoords = generateCoords()
        #     while randomCoords in usedCoords:
        #         randomCoords = generateCoords()
        #     usedCoords.append(randomCoords)

        #     coords = randomCoords
            if self.NUMBER_OF_PLAYERS == 1:
                coords = self.STARTING_POS_1P
            else:
                coords = self.STARTING_POS_2P[i]
                
            self.board[coords[0]][coords[1]] = i + 1
            self.players.append(Player(i + 1, coords, 0, self.W, self.H))

        print('Step: 0')

    def step(self, action):
        self.t += 1
        print('\nStep: ' + str(self.t))
        rewards = [0 for i in range(len(self.players))]
        anticipated_moves = []

        for i in range(len(self.players)):
            player = self.players[i]
            if(player.alive):

                direc = player.direction

                if action[i] == Turn.LEFT.value:
                    direc = (direc + 3) % 4
                    player.direction = direc
                elif action[i] == Turn.RIGHT.value:
                    direc = (direc + 5) % 4
                    player.direction = direc
                elif action[i] != Turn.NO_TURN.value:
                    raise ValueError("invalid turn, expected {0}, {1} or {2}.\
                         got {3}".format(Turn.LEFT.value, Turn.RIGHT.value,
                                         Turn.NO_TURN.value, action[i])
                                     )

                if direc == Direction.NORTH.value:
                    player.coords[1] = player.coords[1] - 1
                elif direc == Direction.EAST.value:
                    player.coords[0] = player.coords[0] + 1
                elif direc == Direction.SOUTH.value:
                    player.coords[1] = player.coords[1] + 1
                elif direc == Direction.WEST.value:
                    player.coords[0] = player.coords[0] - 1
                else:
                    raise ValueError(
                        "invalid direction, expected {0},{1},{2} or {3}. \
                            got {4}".format(
                                Direction.NORTH.value, Direction.EAST.value,
                                Direction.SOUTH.value, Direction.WEST.value, direc)
                    )

                anticipated_moves.append(player.coords)
            else:
                anticipated_moves.append([-1, -1])

        death_counter = 0
        for i in range(len(self.players)):
            player = self.players[i]
            if player.alive:
                if player.coords[0] < 0 or player.coords[0] >= self.W\
                        or player.coords[1] < 0 or player.coords[1] >= self.H\
                        or self.board[player.coords[0]][player.coords[1]] != 0:
                    print("agent is dead")
                    rewards[i] = -30
                    player.alive = False
                    death_counter += 1
                else:
                    player.board[anticipated_moves[i][0]
                                 ][anticipated_moves[i][1]] = 1
                    self.board[anticipated_moves[i][0]
                               ][anticipated_moves[i][1]] = player.ID
                    for j in range(i + 1, len(self.players)):
                        if anticipated_moves[i] == anticipated_moves[j] :
                            print("agents are dead")
                            self.players[i].alive = False
                            rewards[i] = self.DEATH_REWARD
                            self.players[j].alive = False
                            rewards[j] = self.DEATH_REWARD
                            death_counter += 2
                            continue

                    if player.alive:
                        rewards[i] = self.SURVIVE_STEP_REWARD

        # Check if done
        no_of_alive_players = 0
        for i in range(len(self.players)):
            if self.players[i].alive:
                no_of_alive_players += 1
                rewards[i] = death_counter * self.ENEMY_DEATH_REWARD

        if no_of_alive_players == 0:
            done = True
        else:
            done = False

        boards = []
        for i in range(len(self.players)):
            boards.append(self.players[i].board)
        boards = np.array(boards)

        return (boards, rewards, done, " ")

    def reset(self):
        print("RESET ENVIRONMENT")
        self.__init__()
        return self.board

    def render(self, mode='human', close=False):
        print("============")
        for y in range(10):
            line = "|"
            for x in range(10):
                if self.board[x][y] == 0:
                    line += "."
                else:
                    line += str(self.board[x][y])
            line += "|"
            print(line)
        print("============")


class Player():

    def __init__(self, ID, coords, direction, W, H):
        self.ID = ID
        self.coords = coords
        self.newcoords = coords
        self.direction = direction
        self.board = [[0 for x in range(W)] for y in range(H)]
        self.board[self.coords[0]][self.coords[1]] = 1
        self.alive = True
